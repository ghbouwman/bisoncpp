inline size_t RmReduction::idx() const
{
    return d_idx;
}

inline Symbol const *RmReduction::symbol() const
{
    return d_symbol;
}

inline size_t RmReduction::next() const
{
    return d_next;
}

inline bool RmReduction::isForced(RmReduction const &rmReduction)
{
    return rmReduction.d_forced;
}
