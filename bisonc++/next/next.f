inline std::vector<size_t> const &Next::kernel() const
{
    return d_kernel;
}

inline Symbol const *Next::symbol() const
{
    return d_symbol;
}

inline bool Next::hasSymbol(Symbol const *symbol) const
{
    return d_symbol == symbol;
}

inline Symbol const *Next::pSymbol() const
{
    return d_symbol ? d_symbol : d_removed;
}

inline size_t Next::next() const
{
    return d_next;
}

inline void Next::setNext(size_t next)
{
    d_next = next;
}

inline bool Next::inLAset(LookaheadSet const &laSet) const
{
    return laSet >= d_symbol;
}

inline void Next::inserter(std::ostream &(Next::*insertPtr)
                                         (std::ostream &out) const)
{
    s_insertPtr = insertPtr;
}


inline std::ostream &operator<<(std::ostream &out, Next const &next)
{
    return (next.*Next::s_insertPtr)(out);

    // Set by static void inserter(Next::*insertPtr)
    //  to 'transition' or 'transitionKernel'
}
