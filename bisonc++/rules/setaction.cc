#include "rules.ih"

void Rules::setAction(Block const &block, bool defaultAction)
{
    d_defaultAction = defaultAction;
    d_currentProduction->setAction(block);
}

