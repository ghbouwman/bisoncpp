#ifndef _INCLUDED_RRDATA_
#define _INCLUDED_RRDATA_

#include <vector>
#include "../lookaheadset/lookaheadset.h"

    // Data used when processing R/R conflicts. Used by, e.g., StateItem
class RRData
{
    public:
        using Vector = std::vector<RRData>;
        using ConstIter = Vector::const_iterator;

        enum Keep
        {
            KEEP_FIRST,
            KEEP_SECOND,
        };

    private:
        LookaheadSet d_laSet;   // set of LA symbols
        bool d_forced;          // true if one of the two rules is explicitly
                                // kept. 
        size_t d_idx;           // index of item with reduced LA set
        size_t d_kept;          // index of item with kept LA set

    public:
        RRData(LookaheadSet first);

        bool empty() const;
        size_t keepIdx() const;
        size_t reduceIdx() const;
        size_t size() const;
        LookaheadSet const &lookaheadSet() const;
        void setIdx(Keep keep, size_t first, size_t second);
        void setIdx(size_t reduce);     // non-forced

        static bool isForced(RRData const &rrData);
};

#include "rrdata.f"

#endif
