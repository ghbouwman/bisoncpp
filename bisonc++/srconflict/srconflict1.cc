#include "srconflict.ih"

SRConflict::SRConflict(Next::Vector const &nextVector, 
                        StateItem::Vector const &itemVector, 
                        std::vector<size_t> const &reducible)
:
    d_nextVector(nextVector),
    d_itemVector(itemVector),
    d_reducible(reducible), 
    d_lastItemIdx(numeric_limits<size_t>::max()),
    d_lastNext(numeric_limits<size_t>::max())
{}
