inline bool NonTerminal::notUsed()
{
    return s_unused;
}

inline bool NonTerminal::notDefined()
{
    return s_undefined;
}

inline std::ostream &NonTerminal::plainName(std::ostream &out) const
{
    return out << name();
}

inline std::ostream &NonTerminal::nameAndFirstset(std::ostream &out) const
{
    return insName(out) << d_first;
}

inline std::ostream &NonTerminal::value(std::ostream &out) const
{
    return out << std::setw(3) << v_value();
}


inline void NonTerminal::setFirstNr(size_t nr)
{
    s_number = nr;
}

inline void NonTerminal::setNr(NonTerminal *np)
{
    np->d_nr = s_number++;
}

inline size_t NonTerminal::counter() 
{
    return s_counter;
}

inline void NonTerminal::resetCounter()
{
    s_counter = 0;
}

inline void NonTerminal::setNonTerminal(NonTerminal *nonTerminal)
{
    nonTerminal->setType(NON_TERMINAL);
}

inline size_t NonTerminal::firstSize() const
{
    return d_first.setSize();
}

inline std::set<Element const *> const &NonTerminal::firstTerminals() const
{
    return d_first.set();
}

inline void NonTerminal::addProduction(Production *next)
{
    d_production.push_back(next);
}

inline size_t NonTerminal::nProductions() const
{
    return d_production.size();
}

inline void NonTerminal::addEpsilon() 
{
    d_first.addEpsilon();
}

inline Production::Vector &NonTerminal::productions()
{
    return d_production;
}

inline Production::Vector const &NonTerminal::productions() const
{
    return d_production;
}

inline NonTerminal *NonTerminal::downcast(Symbol *sp)
{
    return dynamic_cast<NonTerminal *>(sp);
}

inline NonTerminal const *NonTerminal::downcast(Symbol const *sp)
{
    return dynamic_cast<NonTerminal const *>(sp);
}

inline void NonTerminal::inserter(std::ostream &(NonTerminal::*insertPtr)
                                           (std::ostream &out) const)
{
    s_insertPtr = insertPtr;
}

