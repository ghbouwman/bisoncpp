#ifndef _INCLUDED_ITEM_
#define _INCLUDED_ITEM_

#include <iosfwd>

#include "../production/production.h"
#include "../lookaheadset/lookaheadset.h"

// An Item represents a point in a production rule. The point is indicated by
// its 'dot', which represents the position until where that particular
// production rule has been recognized.

class Item
{
    friend std::ostream &operator<<(std::ostream &out, Item const &item);

    Production const *d_production;         // Pointer to the production rule
                                            // associated with this item

    size_t d_dot;                           // The position in the production
                                            // rule until where the rule has
                                            // been recognized. Dot position 0
                                            // is only found in the start-rule
                                            // and augmented grammar rule and
                                            // represents the position where
                                            // nothing has as yet been
                                            // recognized. 

    static std::ostream &(Item::*s_insertPtr)(std::ostream &out) const;
    
    public:
        using Vector = std::vector<Item>;
        using ConstIter = std::vector<Item>::const_iterator;

        Item();

        Item(Production const *start);      // initial item, starts at the 
                                            // start-production. Dot = 0, 
                                            // Lookahead = EOF

        Item(Item const *item, size_t dot);

        Item(Production const *prod, size_t dot);
                                        //, LookaheadSet const &laSet);

            // see State::beforeDot() to read why this function is only called
            // when d_dot > 0
        Symbol const *dotSymbol() const;        // symbol at the dot (must 
        Symbol const *dotSymbolOr0() const;     // symbol at the dot or 0
        Symbol const *symbolBeforeDot() const;  // symbol before the dot

        Item incDot() const;
                                                // true: returned FirstSet 
                                                // contained (now removed) 
                                                // epsilon
        bool firstBeyondDot(FirstSet *firstSet) const;

        Production const *production() const;   // *rhs() const

        Symbol const *lhs() const;

        bool empty() const;                     // if no size
        bool hasRightOfDot(Symbol const &symbol) const;
        bool operator==(Item const &other) const;
        bool isReducible() const;               // if dot at end
        bool transitsTo(Item const &other) const;

        size_t dot() const;
        size_t productionSize() const;

        static void inserter(std::ostream &(Item::*insertPtr)
                                            (std::ostream &out) const);
        std::ostream &plainItem(std::ostream &out) const;
        std::ostream &pNrDotItem(std::ostream &out) const;

        Symbol const *beyondDotIsNonTerminal() const;   // 0 if not, otherwise
                                                        // the N terminal

    private:
        std::ostream &insert(std::ostream &out, Production const *prod) const;
};

#include "item.f"

#endif
