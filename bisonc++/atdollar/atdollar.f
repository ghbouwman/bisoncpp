inline AtDollar::Pattern AtDollar::pattern() const
{
    return d_pattern;
}
        
inline int AtDollar::nr() const
{
    return d_nr;
}
        
inline bool AtDollar::refByScanner() const
{
    return d_refByScanner;
}
        
inline size_t AtDollar::pos() const
{
    return d_pos;
}
        
inline size_t AtDollar::length() const
{
    return d_length;
}
        
inline size_t AtDollar::lineNr() const
{
    return d_lineNr;
}
        
inline std::string const &AtDollar::text() const
{
    return d_text;
}
        
inline std::string const &AtDollar::tag() const
{
    return d_tag;
}
