#include "generator.ih"

void Generator::preIncludes(std::ostream &out) const
{
    bool preInclude = not d_options.preInclude().empty();
    bool polymorphic =  d_options.polymorphic();
    bool tokenPath = d_options.useTokenPath();

    if (not preInclude and not polymorphic and not tokenPath)
        return;

    key(out);

    if (preInclude)    
        out << "#include " << d_options.preInclude() << '\n';

    if (tokenPath)
        out << "#include \"" << d_options.tokenPath() << "\"\n";


}
