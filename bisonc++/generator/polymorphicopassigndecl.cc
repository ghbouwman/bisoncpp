#include "generator.ih"

void Generator::polymorphicOpAssignDecl(ostream &out) const
{
    key(out);

    for (auto &poly: d_polymorphic)
        out << 
            setw(8) << "" <<
                "SType &operator=(" << poly.second << " const &value);\n" <<
            setw(8) << "" <<
                "SType &operator=(" << poly.second << " &&tmp);\n"
            "\n";

    out << 
        setw(8) << "" <<
             "SType &operator=(EndPolyType_ const &value);\n" <<
        setw(8) << "" <<
            "SType &operator=(EndPolyType_ &&tmp);\n"
        "\n";
}



